# Projet_GCC_TSP

## Description
Ici, j'ai travaillé sur le problème du voyageur du commerce. Pour cela, j'ai utilisé les données issues de TSPL-Lib et plus précisément les données du Djibuti qui contient uniquement 38 villes. 

## Données 
Le lien pour les données est le suivant : https://www.math.uwaterloo.ca/tsp/world/countries.html 

## Les différents scripts 
- tsp_solveur_CP_param.py : est le code CP-Sat paramétré avec les paramètres 'time_limit' et 'linearizaton_level'
- tsp_solveur_CP.py : est le code CP-Sat non paramétré 
- tsp_solveur_IT. py: est le code Iter-tools 
- tsp_solveur_OR.py : est le code OR-Tools 


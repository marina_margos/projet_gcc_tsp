from itertools import permutations

# Fonction pour créer la matrice de distance euclidienne à partir des coordonnées
def create_data_model():
    data = {}
    data['distance_matrix'] = [
        [
            int(round(
                ((coord1[0] - coord2[0])**2 + (coord1[1] - coord2[1])**2)**0.5
            ))
            for coord2 in node_coords
        ]
        for coord1 in node_coords
    ]
    data['num_vehicles'] = 1
    data['depot'] = 0  # Le point de départ est le premier nœud dans l'ensemble de données
    return data

# Lecture des coordonnées des nœuds à partir du jeu de données
node_coords = []
with open('data/dj38.tsp') as file:
    lines = file.readlines()[8:19]  # Commence à la ligne NODE_COORD_SECTION
    reading_coords = False
    for line in lines:
        if line.strip() == 'NODE_COORD_SECTION':
            reading_coords = True
            continue
        elif line.strip() == '':  # Fin de la section des coordonnées
            reading_coords = False
            break
        elif reading_coords:
            values = line.split()
            if len(values) == 3:
                _, x, y = map(float, values)
                node_coords.append((x, y))
            else:
                print(f"Ignoré une ligne avec un nombre incorrect de valeurs: {line}")

# Afficher les coordonnées des nœuds pour vérification
print(node_coords)


def solve_tsp_bruteforce():
    # Créer le modèle de données
    data = create_data_model()

    # Générer toutes les permutations des villes
    all_permutations = permutations(range(len(data['distance_matrix'])))

    best_route = None
    best_distance = float('inf')

    # Parcourir toutes les permutations
    for idx, permutation in enumerate(all_permutations):
        current_route = list(permutation) + [permutation[0]]  # Revenir à la ville de départ
        current_distance = sum(data['distance_matrix'][i][j] for i, j in zip(current_route[:-1], current_route[1:]))

        # Imprimer chaque permutation générée
        print(f'Permutation {idx + 1}: {current_route}, Distance: {current_distance}')

        # Mettre à jour la meilleure route si la distance est plus courte
        if current_distance < best_distance:
            best_distance = current_distance
            best_route = current_route

    # Afficher la meilleure solution à la fin
    print('\nMeilleure solution (bruteforce):')
    print('Chemin optimal:', best_route)
    print('Distance totale:', best_distance)

# Appeler la fonction pour résoudre le problème TSP en utilisant une méthode exhaustive
solve_tsp_bruteforce()

from ortools.sat.python import cp_model
import math

COORDINATES = [
    (11003.611100, 42102.500000),
    (11108.611100, 42373.888900),
    (11133.333300, 42885.833300),
    (11155.833300, 42712.500000),
    (11183.333300, 42933.333300),
    (11297.500000, 42853.333300),
    (11310.277800, 42929.444400),
    (11416.666700, 42983.333300),
    (11423.888900, 43000.277800)
]

def calculate_distance(coord1, coord2):
    """Calculates the Euclidean
     distance between two coordinates."""
    return math.sqrt((coord1[0] - coord2[0])**2 + (coord1[1] - coord2[1])**2)

# Définissez la matrice de distance en calculant les distances entre chaque paire de points.
MATRICE_DISTANCE = [
    [calculate_distance(coord1, coord2) for coord2 in COORDINATES]
    for coord1 in COORDINATES
]

# Utilisez maintenant MATRICE_DISTANCE dans votre code existant.

def main(time_limit=10, linearization_level=2):
    num_nodes = len(MATRICE_DISTANCE)
    all_nodes = range(num_nodes)
    print('Num nodes =', num_nodes)

    model = cp_model.CpModel()

    obj_vars = []
    obj_coeffs = []

    # Créer la contrainte du circuit.
    arcs = []
    arc_literals = {}
    for i in all_nodes:
        for j in all_nodes:
            if i == j:
                continue

            lit = model.NewBoolVar('%i suit %i' % (j, i))
            arcs.append([i, j, lit])
            arc_literals[i, j] = lit

            obj_vars.append(lit)
            obj_coeffs.append(MATRICE_DISTANCE[i][j])

    model.AddCircuit(arcs)

    # Minimiser la somme pondérée des arcs.
    model.Minimize(
        sum(obj_vars[i] * obj_coeffs[i] for i in range(len(obj_vars))))

    # Résoudre et imprimer la solution.
    solver = cp_model.CpSolver()
    solver.parameters.log_search_progress = True
    solver.parameters.max_time_in_seconds = time_limit
    # Pour bénéficier de la linéarisation de la contrainte du circuit.
    solver.parameters.linearization_level = linearization_level

    solver.Solve(model)
    print(solver.ResponseStats())

    current_node = 0
    str_route = '%i' % current_node
    route_is_finished = False
    route_distance = 0
    while not route_is_finished:
        for i in all_nodes:
            if i == current_node:
                continue
            if solver.BooleanValue(arc_literals[current_node, i]):
                str_route += ' -> %i' % i
                route_distance += MATRICE_DISTANCE[current_node][i]
                current_node = i
                if current_node == 0:
                    route_is_finished = True
                break

    print('Route:', str_route)
    print('Distance parcourue:', route_distance)

if __name__ == '__main__':
    main(time_limit=10, linearization_level=2)

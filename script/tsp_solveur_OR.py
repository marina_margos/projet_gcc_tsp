from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp

# Fonction pour créer la matrice de distance euclidienne à partir des coordonnées
def create_data_model():
    data = {}
    data['distance_matrix'] = [
        [
            int(round(
                ((coord1[0] - coord2[0])**2 + (coord1[1] - coord2[1])**2)**0.5
            ))
            for coord2 in node_coords
        ]
        for coord1 in node_coords
    ]
    data['num_vehicles'] = 1
    data['depot'] = 0  # Le point de départ est le premier nœud dans l'ensemble de données
    return data

# Lecture des coordonnées des nœuds à partir du jeu de données
node_coords = []
with open('data/dj38.tsp') as file:
    lines = file.readlines()[8:20]  # Commence à la ligne NODE_COORD_SECTION
    reading_coords = False
    for line in lines:
        if line.strip() == 'NODE_COORD_SECTION':
            reading_coords = True
            continue
        elif line.strip() == '':  # Fin de la section des coordonnées
            reading_coords = False
            break
        elif reading_coords:
            values = line.split()
            if len(values) == 3:
                _, x, y = map(float, values)
                node_coords.append((x, y))
            else:
                print(f"Ignoré une ligne avec un nombre incorrect de valeurs: {line}")

# Afficher les coordonnées des nœuds pour vérification
print(node_coords)


def solve_tsp():
    # Créer le modèle de données
    data = create_data_model()

    # Créer le solveur
    manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']),
                                           data['num_vehicles'], data['depot'])
    routing = pywrapcp.RoutingModel(manager)

    # Définir la distance (coût) entre les nœuds
    def distance_callback(from_index, to_index):
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)

    # Définir le coût du transit (distance entre les nœuds)
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

    # Définir la stratégie pour la première solution
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC
    )

    # Activer la recherche locale guidée
    search_parameters.local_search_metaheuristic = (
        routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH
    )

    # Limiter le temps de recherche
    search_parameters.time_limit.seconds = 60

    # Résoudre le problème TSP
    solution = routing.SolveWithParameters(search_parameters)

    # Afficher la solution
    if solution:
        print_solution(manager, routing, solution)
    else:
        print("Pas de solution trouvée.")

def print_solution(manager, routing, solution):
    print('Coût de la solution: {}'.format(solution.ObjectiveValue()))
    index = routing.Start(0)
    plan_output = 'Chemin optimal:\n'
    route_distance = 0
    while not routing.IsEnd(index):
        plan_output += ' {} ->'.format(manager.IndexToNode(index))
        previous_index = index
        index = solution.Value(routing.NextVar(index))
        route_distance += routing.GetArcCostForVehicle(previous_index, index, 0)
    plan_output += ' {}\n'.format(manager.IndexToNode(index))
    print(plan_output)
    print('Distance totale: {}'.format(route_distance))

# Appeler la fonction pour résoudre le problème TSP
solve_tsp()
